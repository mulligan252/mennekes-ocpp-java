/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Paul Mulligan
 */
package Mennekes.ocpp.java;

import javax.xml.soap.*;

public class LoadManagementController 
{
    
    int CURRENT_LIMIT_CHARGER = 50;
    MennekesRateControlClient mennekes_client;
    
    public LoadManagementController()
    {
        mennekes_client = new MennekesRateControlClient();
    }
    
    public void set_current(int current)
    {
        if ( current > CURRENT_LIMIT_CHARGER )
        {
            current = CURRENT_LIMIT_CHARGER;
        }
        else if (current < 0)
        {
            current = 0;
        }
   
        try 
        {
            mennekes_client.setChargingRate(current * 10);
        }
        catch (SOAPException se)
        {
            MennekesRateControlClient.log.error("Exception on trying to set the charging rate from LoadmanagementController");
            se.printStackTrace();
        }
        
    }
}
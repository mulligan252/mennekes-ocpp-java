/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mennekes.ocpp.java;

import java.time.LocalTime;
import java.util.List;
import javax.xml.soap.SOAPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author Paul Mulligan
 */
public class Main 
{
    static String HOST_ACU             = "http://10.8.0.18/index_main";
    static String HOST_ACU_OCPP        = "http://192.168.1.10:13000";

    static String ACU_USER             = "admin";
    static String ACU_PASS             = "admin";

    static String chargeBoxID          = "Test";
    
    static double charge_station_power = 0.0;
    static double max_charge_power     = 0.0;
    static double max_charge_current   = 0.0;
    static double site_load            = 0.0;
    
    static int current_limit_charger   = 83;
    static int power_limit             = 200;
    
    static String control_type         = "simple";
    
    static double sma_meter_power      = 30;       // This value will be the meter power read from the SMA data point!!!!!!!!!!!!!!!!!!!
    
    static boolean debug               = true;
    static final Logger log            = LoggerFactory.getLogger(Main.class);
    
    static int control_active          = 1;
 
    static MennekesRateControlClient lama_client;
    static ACUConnection             acuCon;
    static PIDController             pidController;
    static List<Meter>               acu_values;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        System.out.println("Running Mennekes Application..");
        
        lama_client   = new MennekesRateControlClient(HOST_ACU_OCPP, chargeBoxID);
        acuCon        = new ACUConnection(HOST_ACU, ACU_USER, ACU_PASS);
        pidController = new PIDController(1, 0.05, 0);
        
        while (true)
        {
            // sleep for 10 seconds
            try 
            {
                Thread.sleep(10000);
            }
            catch (InterruptedException ie)
            {
                ie.printStackTrace();

            }
            
            acu_values           = acuCon.update_avg_power(acu_values);     // Get latest power usage from the acu
            charge_station_power = 0.0;
            
            for (int i = 0; i < acu_values.size(); ++i)
            {
                charge_station_power += acu_values.get(i).power;
            }
            
            
            // simple control type
            if (control_type == "simple")
            {
                site_load        = sma_meter_power - charge_station_power;
                max_charge_power = power_limit - site_load;
                max_charge_power = (max_charge_power < 0)? 0 : max_charge_power;
                
                //Calculate max current based on three phase charging and send limit
                double current_limit    = max_charge_power*(1000/3/230); //3 Phase 230V L-N Voltage
                
                if (current_limit > current_limit_charger)
                {
                    current_limit = current_limit_charger;
                }
                else if (current_limit < 0)
                {
                    current_limit = 0;
                }
                
                 if (debug)
                 {
                     long millis=System.currentTimeMillis();  
                     java.util.Date date=new java.util.Date(millis);
                     log.info(date.toString());
                     log.info("Current Total Load : " + sma_meter_power);
                     log.info("Current Charge Station Load : " + charge_station_power + "KW");
                     log.info("Current Site Load : " + site_load + "KW");
                     log.info("Current Charge Power limitation : " + max_charge_power + "KW");
                     log.info("Current Charge Current limitation : " + max_charge_current + "KW");
                 }
                
                 if (control_active > 0 && current_limit != max_charge_current)
                 {
                     max_charge_current=current_limit;
                     set_current(max_charge_current);
                 }
            }
            
            // pid control type
            else if (control_type == "pid")
            {
                if (power_limit != pidController.setPoint)
                {
                    pidController.clear();
                    pidController.setPoint = power_limit;
                }
                pidController.update(sma_meter_power );
                
            }
        }
        
    }
    
    public static void set_current(double current)
    {
        int curr = (int)Math.round(current);
        
        if (curr > current_limit_charger )
        {
            curr = current_limit_charger;
        }
        else if (curr < 0)
        {
            curr = 0;
        }
        
        try 
        {
            lama_client.setChargingRate(curr * 10);
        }
        catch(SOAPException se)
        {
            log.error("Exception on trying to set the charging rate!!");
            se.printStackTrace();
        }
        
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mennekes.ocpp.java;

/**
 *
 * @author Paul Mulligan
 * 
 *
 */

/**
 * Enum reflected values of "ConfigurationStatus" defined in OCPP Specification 1.5, Section 7.13
 * 
 * <ul>
 * <li>Accepted: Configuration key supported and setting has been changed.
 * <li>Rejected: Configuration key supported, but setting could not be changed.
 * <li>NotSupported: Configuration key is not supported.
 * </ul>
 */
public enum ChangeConfigurationStatus 
{
    Accepted,Rejected,NotSupported
}

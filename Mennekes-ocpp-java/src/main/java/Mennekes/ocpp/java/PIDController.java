/**
 *
 * @author Paul Mulligan
 */

package Mennekes.ocpp.java;

import java.time.LocalTime;  


public class PIDController
{
    protected double kp           = 0.2;
    protected double ki           = 0.0;
    protected double kd           = 0.0;
    
    long current_time             = 0;
    long last_time                = 0;
    
    protected double sample_time  = 0.0;
    
    
    protected double setPoint     = 0.0;
    protected double output       = 0.0;
    
    protected double pTerm        = 0.0;
    protected double iTerm        = 0.0;
    protected double dTerm        = 0.0;
    protected double last_error   = 0.0;

        // Windup Guard
    protected double int_error    = 0.0;
    protected double windup_guard = 20.0;
    
    
    public PIDController (double p, double i, double d )
    {
        kp = p;
        ki = i;
        kd = d;
        
        current_time = System.currentTimeMillis();
         
        sample_time  = 0.00;
        
        last_time    = current_time;
        setPoint     = 0.0;
        output       = 0;

        clear();
    }
    
    public void clear()
    {
        //Clears PID computations and coefficients
        setPoint     = 0.0;

        pTerm        = 0.0;
        iTerm        = 0.0;
        dTerm        = 0.0;
        last_error   = 0.0;

        // Windup Guard
        int_error    = 0.0;
        windup_guard = 20.0;

        output       = 0.0;
        
    }
    
    public void update(double feedback_value )
    {
        /*Calculates PID value for given reference feedback
        .. math::
            u(t) = K_p e(t) + K_i \int_{0}^{t} e(t)dt + K_d {de}/{dt}
        .. figure:: images/pid_1.png
           :align:   center
           Test PID with Kp=1.2, Ki=1, Kd=0.001 (test_pid.py)
        */
        
        double error       = setPoint - feedback_value;
        current_time       = System.currentTimeMillis();
        
        long delta_time    = current_time - last_time;
        double delta_error = error - last_error;
        
        if (delta_error >= sample_time)
        {
            pTerm  = kp * error;
            iTerm += error * (double)delta_time;
            
            if (iTerm < (windup_guard * -1) )
            {
                iTerm = (windup_guard * -1);
            }
            else if (iTerm > windup_guard)
            {
                iTerm = windup_guard;
            }
            
            dTerm = 0;
            
            if (delta_time > 0)
            {
                dTerm = delta_error / (double)delta_time;
            }
            
            // Remember last time and last error for next calculation
            last_time = current_time;
            last_error = error;

            output = pTerm + (ki * iTerm) + ( kd * dTerm);
        }
        
    }
    
    void setKp(double proportional_gain)
    {
        //Determines how aggressively the PID reacts to the current error with setting Proportional Gain
        kp = proportional_gain;
    }
    
    void setKi(double integral_gain)
    {
        //Determines how aggressively the PID reacts to the current error with setting Integral Gain
        ki = integral_gain;
    }    
    
    void setKd(double derivative_gain)
    {
        //Determines how aggressively the PID reacts to the current error with setting Derivative Gain
        kd = derivative_gain;
    }  


    void setWindup(double windup)
    {
        /*Integral windup, also known as integrator windup or reset windup,
        refers to the situation in a PID feedback controller where
        a large change in setpoint occurs (say a positive change)
        and the integral terms accumulates a significant error
        during the rise (windup), thus overshooting and continuing
        to increase as this accumulated error is unwound
        (offset by errors in the other direction).
        The specific problem is the excess overshooting.
        */
        windup_guard = windup; 
    }  
   

    void setSampleTime(double time)
    {
        /*PID that should be updated at a regular interval.
        Based on a pre-determined sampe time, the PID decides if it should compute or return immediately.
        */
    }
        
}


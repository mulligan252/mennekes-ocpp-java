/***************************************************************************
 *              SMA Solar Technology AG, 34266 Niestetal, Germany
 ***************************************************************************
 *
 *  Copyright (c) 2016
 *  SMA Solar Technology AG
 *  All rights reserved.
 *  This design is confidential and proprietary of SMA Solar Technology AG.
 *
 ***************************************************************************/

package Mennekes.ocpp.java;


import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.logging.Level;

import javax.xml.soap.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.NodeList;

/**
 * This client sends a SOAP request (conforming to OCPP 1.5 SOAP) to a Mennekes charge point and requests it
 * to change its charging rate (in amps) using the LaMa_ConnectionRate
 * parameter.
 *
 * TODO: does not handle compressed requests/responses
 * @author paul.mulligan
 */
public class MennekesRateControlClient 
{
	
    protected boolean debug = false;

    protected int     chargingRate;
    protected String  SOAPAction = "/ChangeConfiguration";
	
    protected String  centralSystemRootURI = null;
    protected String  chargeBoxIdentity    = null;
    
    public static final Logger log = LoggerFactory.getLogger(MennekesRateControlClient.class);
	
    // These following members can probably be removed in the end
    protected final Thread   thread;   // perhaps not required finally as we are not doing any explicit polling
    private double meterConsumptionValue_test = 100; // Just for hard-code testing 
    protected int  maxPower;    // only used with run() method
	
	
    /**
    * Default Constructor
    *
    */
    public MennekesRateControlClient() 
    {
        thread = new Thread(() -> run());
    }
	 
    /**
     * Argument Constructor
     *
     * @param centralSystemRootURI
     * @param chargeBoxIdentity
     */
    public MennekesRateControlClient(String centralSystemRootURI, String chargeBoxIdentity) 
    {
    	this.centralSystemRootURI = centralSystemRootURI;
        this.chargeBoxIdentity = chargeBoxIdentity;
        
        thread = new Thread(() -> run());
    }
    
    /**
     * run() : The method which runs when thread.start() is invoked !! THIS IS NOT USED WHEN USING THE CALLBACK METHOD onUpdateMeterData in ConsumptionMeterData
     *
     * 
     */
    
    private void run()
    {  
    	if ( meterConsumptionValue_test > maxPower)
        {
            log.info("Measurement.Metering.PCCMs.PlntCsmpW is greater than Pmax - Disabling charging!!!!!");
            try 
            {
            	log.info("Setting charging rate to : " + chargingRate);
                System.out.println("Setting charging rate to : " + chargingRate);
                ChangeConfigurationStatus status = setChargingRate(chargingRate);
                log.info("Response Status: " + status.toString());
            
            }
            catch ( SOAPException se) 
            {
                log.error("SOAPException occured on attempting to set the charging rate!");
                se.printStackTrace();
            }
            catch ( Exception e) 
            {
                log.error("Exception occured on attempting to set the charging rate!");
                e.printStackTrace();
            }
        }
        else
        {
            log.info("Measurement.Metering.PCCMs.PlntCsmpW is less than Pmax - Charging OK!!!");
        }               
    }
    
    /**
     * start() : Starts the thread running 
     *
     * 
     */
    public void start()
    {
    	thread.start();
    }
    

    /**
     * Sets the charging rate in amps
     *
     * @param newChargingRateAmps
     */
    public ChangeConfigurationStatus setChargingRate(int newChargingRateAmps) throws SOAPException 
    {
        ChangeConfigurationStatus status = callSoapWebService(this.centralSystemRootURI, newChargingRateAmps);
    	
        if (debug)
        {
            log.info("STATUS IS : " + status );
        }
        
        return status;
    }

    /**
     * Calls the SOAP change configuration service and processes the response
     *
     * @param soapEndpointUrl
     * @param rate
     */
    private ChangeConfigurationStatus callSoapWebService(String soapEndpointUrl, int rate) throws SOAPException 
    {
        SOAPConnection soapConnection = null;

        log.info("Calling SOAP service at: " + soapEndpointUrl);
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();

        soapConnection = soapConnectionFactory.createConnection();
        SOAPMessage soapRequest = createSOAPRequest(rate);
        
        MimeHeaders mimeHeaders = soapRequest.getMimeHeaders();
        
        
        mimeHeaders.addHeader("Content-Type", "application/soap+xml;charset=UTF-8;action=\"" + SOAPAction + "\"");

        if (debug)
        {
        	/* Print the request message, just for debugging purposes */
            System.out.println("ABOUT TO Print the request");
            System.out.println("");
            System.out.println("");
            printPrettyPrintedSoapMessage(soapRequest);
            System.out.println("");
            System.out.println("");
            System.out.println("");
        }
        

        // Send SOAP Message to SOAP Server
        SOAPMessage soapResponse = soapConnection.call(soapRequest, soapEndpointUrl);

        if (debug)
        {
        	// log the SOAP Response
            System.out.println("ABOUT TO Print the response!!!!!!");
            System.out.println("");
            System.out.println("");
            printPrettyPrintedSoapMessage(soapResponse);
            System.out.println("");
            System.out.println("");
        	
        }
        
    	SOAPBody soapBody = soapResponse.getSOAPBody();
        Name bodyName = soapResponse.getSOAPPart().getEnvelope().createName("changeConfigurationResponse", "ns", "urn://Ocpp/Cp/2012/06/");
        NodeList list = soapBody.getElementsByTagName("cp:status");
        log.info("Response: " + list.item(0).getTextContent());
        soapConnection.close();
        return ChangeConfigurationStatus.valueOf(list.item(0).getTextContent());

    }

    /**
     * Assembles the SOAP request object to do a changeConfigurationRequest
     *
     * @param rate
     * @return
     * @throws Exception
     */
    private SOAPMessage createSOAPRequest(int rate) throws SOAPException 
    {

        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPFactory soapFactory = SOAPFactory.newInstance();
        
        SOAPMessage soapMessage = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
     
        SOAPEnvelope envelope = soapPart.getEnvelope();
        SOAPHeader header = soapMessage.getSOAPHeader(); 
        
        
        envelope.addNamespaceDeclaration("SOAP-ENV", "http://www.w3.org/2003/05/soap-envelope/");
        envelope.addNamespaceDeclaration("wsa", "http://www.w3.org/2005/08/addressing");
        envelope.addNamespaceDeclaration("cs", "urn://Ocpp/Cp/2012/06/");
        
        
        Name headerName1 = soapFactory.createName("chargeBoxIdentity", "cs", "urn://Ocpp/Cp/2012/06/");
        SOAPHeaderElement headerElement1 = header.addHeaderElement(headerName1);
        headerElement1.addTextNode(this.chargeBoxIdentity);
        
        Name headerName2 = soapFactory.createName("Action", "wsa", "http://www.w3.org/2005/08/addressing");
        SOAPHeaderElement headerElement2 = header.addHeaderElement(headerName2);
        headerElement2.addTextNode(SOAPAction);
        
        Name headerName3 = soapFactory.createName("MessageID", "wsa", "http://www.w3.org/2005/08/addressing");
        SOAPHeaderElement headerElement3 = header.addHeaderElement(headerName3);
      
        headerElement3.addTextNode("Fake OCPP571322528896");

        SOAPBody soapBody = soapMessage.getSOAPBody();

        SOAPElement soapBodyElem = soapBody.addChildElement("changeConfigurationRequest", "cs");
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("key", "cs");
        soapBodyElem1.addTextNode("LaMa_ConnectionRate");
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("value", "cs");
        soapBodyElem2.addTextNode(Integer.toString(rate));
        
        
        soapMessage.saveChanges();

        return soapMessage;

    }

    /**
     * Pretty prints a SOAP message for debugging purposes
     *
     * @param soapMessage
     * @return
     * @throws TransformerFactoryConfigurationError
     * @throws TransformerConfigurationException
     * @throws SOAPException
     * @throws TransformerException
     */
    private void printPrettyPrintedSoapMessage(final SOAPMessage soapMessage) 
    {
        try 
        {
        	final TransformerFactory transformerFactory = TransformerFactory.newInstance();
            final Transformer transformer = transformerFactory.newTransformer();

            Iterator mimeHeadersIter = soapMessage.getMimeHeaders().getAllHeaders();
            while (mimeHeadersIter.hasNext()) 
            {
                MimeHeader header = (MimeHeader) mimeHeadersIter.next();
                System.out.println("MIME> " + header.getName() + "=" + header.getValue());
            }

            // Format it
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

            final Source soapContent = soapMessage.getSOAPPart().getContent();

            final ByteArrayOutputStream streamOut = new ByteArrayOutputStream();
            final StreamResult result = new StreamResult(streamOut);
            transformer.transform(soapContent, result);

            System.out.println(streamOut.toString());
    	} 
        catch (TransformerException ex) 
        {
            java.util.logging.Logger.getLogger(MennekesRateControlClient.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (SOAPException ex) 
        {
            java.util.logging.Logger.getLogger(MennekesRateControlClient.class.getName()).log(Level.SEVERE, null, ex);
        }
	}

}

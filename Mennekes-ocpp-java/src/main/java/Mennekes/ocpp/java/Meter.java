/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mennekes.ocpp.java;

import java.util.List;

/**
 *
 * @author Paul Mulligan
 */
public class Meter 
{
    public int     id;
    public String  name;
    public String  serial;
    public String  status;
    public double  chargingCurrent;
    public String  address;
    public double  meterReading;
    public long    timestamp_ms;
    public double  power;

    public Meter(List<String> data) 
    {
        this.id = Integer.parseInt(data.get(0));
        this.name = data.get(1);            
        this.serial = data.get(2);
        this.status = data.get(3);
        this.chargingCurrent = Double.parseDouble(data.get(4));
        this.address = data.get(5);
        this.meterReading = Double.parseDouble(data.get(6));
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

/**
 *
 * @author Paul Mulligan
 */
package Mennekes.ocpp.java;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


public class ACUConnection 
{
    private String URL_;      //"http://10.8.0.18:84/index_main"
    private String user_;     //"admin"
    private String password_; //"admin"
    
    public ACUConnection(String URL, String user, String password)
    {
        this.URL_      = URL;
        this.user_     = user;
        this.password_ = password;
    }
    
    public List<Meter> update_avg_power (List<Meter> old_values)
    {
        List<Meter> new_values = receive_data();
        
        for (int i = 0; i < new_values.size(); i++)
        {
            Meter scu_old = old_values.get(i);
            Meter scu_new = new_values.get(i);
            
            double new_meter_value = scu_new.meterReading;
            double old_meter_value = scu_old.meterReading;
            
            long new_timestamp = scu_new.timestamp_ms;
            long old_timestamp = scu_new.timestamp_ms;
            
            double value_diff = new_meter_value - old_meter_value;
            
            value_diff = (value_diff >= 0)? value_diff : 0;  // If the meter value difference is less than zero , set it to zero 
            
            scu_new.power = value_diff/(new_timestamp - old_timestamp);  // THIS MUST BE REVISED - NOT CORRECT CALCULATION
        }
        
        return new_values;
        
    }
   
    public List<Meter> receive_data()
    {
        List<Meter> meters = new ArrayList();
        URL url = null;
        try
        {
            url = new URL(URL_);
            String encoding = Base64.getEncoder().encodeToString(("admin:admin").getBytes("UTF-8"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setRequestProperty ("Authorization", "Basic " + encoding);
            InputStream content = (InputStream)connection.getInputStream();
            BufferedReader in   = new BufferedReader (new InputStreamReader (content));
            String line;
            String html_string = "";
            while ((line = in.readLine()) != null) 
            {
                //System.out.println(line);
                html_string += line;
            }
            
            Document doc = Jsoup.parse(html_string);
            Elements trs = doc.select("table.scu_list tr");
            //System.out.println(table);
            //calculate_data(table);
            
            for(int i = 1; i< trs.size(); i++)
            {
                List<String> row = trs.get(i).select("th").stream().map(e -> e.text()).collect(Collectors.toList());
                Meter meter = new Meter(row);
                meter.timestamp_ms = System.currentTimeMillis();
                meters.add(meter);
            }
        }
        catch (MalformedURLException mue)
        {
            Logger.getLogger(ACUConnection.class.getName()).log(Level.SEVERE, null, mue);
        } 
        catch (UnsupportedEncodingException uee) 
        {
            Logger.getLogger(ACUConnection.class.getName()).log(Level.SEVERE, null, uee);
        } 
        catch (IOException ioe) 
        {
            Logger.getLogger(ACUConnection.class.getName()).log(Level.SEVERE, null, ioe);
        }
        
        return meters;
         
    } 
    
}

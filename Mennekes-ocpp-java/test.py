import requests
from bs4 import BeautifulSoup

def calculate_data(table):
    print("In calculate data")
    data=[]
    rows = table.find_all('tr')
    for row in rows[1:len(rows)]:
        fields=row.find_all('th')
        field_number=0;
        scu={}
        for field in fields:
            if field_number==1:
                scu['name']=field.text
            if field_number==2:
                scu['serial']=field.text
            if field_number==3:
                scu['state']=field.text
            if field_number==4:
                scu['max_current']=field.text
            if field_number==6:
                scu['meter_value']=field.text
            field_number+=1
        scu['timestamp']=time.time()
        data.append(scu)
    return data
    
    
def receive_data():
    URL = "http://10.8.0.18/index_main"

    ACU_USER="admin"
    ACU_PASS="admin"
    try:
        r = requests.get(URL, auth=(ACU_USER,ACU_PASS), verify=False)
        soup = BeautifulSoup(r.text, 'html.parser')
        table = soup.find('table', attrs={'class':'scu_list'})
        print(table)
        return calculate_data(table)
    except Exception as e:
        return 0
        
def update_avg_power():
    new_data=receive_data()
    output=[]
    idx=0
    try:
        for scu in new_data:
            #scu_old=old_values[idx]
            new_meter_value=float(scu['meter_value'])
            #old_meter_value=float(scu_old['meter_value'])
            new_timestamp=scu['timestamp']
            #old_timestamp=scu_old['timestamp']

            diff = new_meter_value
            if diff < 0: diff=0

            scu['power']=round((diff)/(new_timestamp-old_timestamp)*3600,2)
            value = round((diff)/(new_timestamp-old_timestamp)*3600,2)
            print("value is : " + str(value) )
            idx+=1
            output.append(scu)
    except Exception as e:
        print("exception")
        out=[]
        #scu['power']=0
        #out.append(scu)
        return out

    return output
            
update_avg_power()